package minhphu.english.slang.ui.listword.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import minhphu.english.slang.data.database.entities.Word
import minhphu.english.slang.ui.listword.WordFragment

class ListWordPagerAdapter(fm: FragmentManager, private val wordList: List<Word>) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? = WordFragment.newInstance(wordList[position])

    override fun getCount(): Int = wordList.size
}
