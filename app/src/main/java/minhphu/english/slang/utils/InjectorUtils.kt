package minhphu.english.slang.utils

import android.content.Context
import minhphu.english.slang.data.database.AppDatabase
import minhphu.english.slang.data.database.daos.WordDao

object InjectorUtils {

    fun provideWordDao(context: Context): WordDao {
        return AppDatabase.getInstance(context).wordDao()
    }
}
