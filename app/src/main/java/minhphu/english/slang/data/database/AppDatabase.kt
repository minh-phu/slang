package minhphu.english.slang.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import com.huma.room_for_asset.RoomAsset
import minhphu.english.slang.data.database.AppDatabase.Companion.DATABASE_VERSION
import minhphu.english.slang.data.database.daos.WordDao
import minhphu.english.slang.data.database.entities.Word


@Database(
    entities = [ (Word::class)],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun wordDao(): WordDao

    companion object {
        const val DATABASE_VERSION = 2
        private const val DATABASE_NAME = "assets.db"

        private var sInstance: AppDatabase? = null
        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (sInstance == null) {
                sInstance = RoomAsset
                    .databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        DATABASE_NAME
                    )
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()
            }
            return sInstance as AppDatabase
        }
    }
}